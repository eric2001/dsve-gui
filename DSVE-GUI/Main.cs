﻿/*
 *  Project Name:  DSVE-GUI
 *  Copyright 2008 Eric Cavaliere <rwatcher@watcherdatabase.tk>
 *  License:  GPL Version 3
 *  Project Start Date: 23 April 2008
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DSVE_GUI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            loadPreferences();
            this.Closing += new CancelEventHandler(savePreferences);
        }

        private void buttonInputBrowse_Click(object sender, EventArgs e)
        {
            // Create an OpenFileDialog and allow the user to select an 
            //  input video.  Automatically provide INPUTFILE.dsv as a 
            //  suggested output file name.
            OpenFileDialog inputFile = new OpenFileDialog();
            DialogResult inputResult;

            inputFile.Title = "Please Select a Video File";
            inputResult = inputFile.ShowDialog();
            if (inputResult == DialogResult.OK)
            {
                txtInput.Text = inputFile.FileName;
                txtOutput.Text = inputFile.FileName + ".dsv";
            }
        }

        private void buttonOutputBrowse_Click(object sender, EventArgs e)
        {
            // Create a new SaveFileDialog and allow the user to
            //  specify an output file.  If the output file does not
            //  end in .dsv, automatically append this file extension.
            SaveFileDialog outputFile = new SaveFileDialog();
            DialogResult outputResult;

            outputFile.Title = "Please Specify an Output File";
            outputResult = outputFile.ShowDialog();
            if (outputResult == DialogResult.OK)
            {
                txtOutput.Text = outputFile.FileName;
                if (!txtOutput.Text.EndsWith(".dsv"))
                    txtOutput.Text += ".dsv";
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            // If both txtInput and txtOutput have a value, then add
            //  this combination of input/output files to the videos list.
            //  Clear txtInput and txtOuput for the next video.
            // If either txtInput or txtOutput does not have a value, 
            //  display an error.
            if ((txtInput.Text.Length > 0) && (txtOutput.Text.Length > 0))
            {
                ListViewItem newVideo = new ListViewItem();
                newVideo.Text = txtInput.Text;
                newVideo.SubItems.Add(txtOutput.Text);
                newVideo.SubItems.Add(txtOptionalParams.Text.Trim());
                if (checkUse2Encoders.Checked == true)
                {
                    string encoderParameters = txtSecondEncoderParams.Text;
                    encoderParameters = encoderParameters.Replace("%SOURCEVIDEO%", txtInput.Text);
                    encoderParameters = encoderParameters.Replace("%TEMPVIDEO%", txtTempVideoFile.Text);
                    newVideo.Text = txtTempVideoFile.Text;
                    newVideo.SubItems.Add(txtSecondVideoEncoder.Text);
                    newVideo.SubItems.Add(encoderParameters);
                }
                else
                {
                    newVideo.SubItems.Add("");
                    newVideo.SubItems.Add("");
                }
                lstVideos.Items.Add(newVideo);
                txtInput.Text = "";
                txtOutput.Text = "";
            }
            else
                MessageBox.Show("Please Select Both an Input and an Output File.", "Error", MessageBoxButtons.OK);
        }

        private void buttonEncodeVideos_Click(object sender, EventArgs e)
        {
            // Use DSVideo to encode the videos.
            int counter = 0;
            string dsvideoParams = "";

            // Make sure there's something to encode.
            if (lstVideos.Items.Count == 0)
            {
                MessageBox.Show("Please add at least one video first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Don't allow new videos to be added while encoding.
            buttonAdd.Enabled = false;

            // Don't allow the encode button to be pressed more then once.
            buttonEncodeVideos.Enabled = false;

            // Loop through each video on the lstVideos list.
            foreach (ListViewItem oneVideo in lstVideos.Items)
            {
                // Show the user which video is being encoded.
                counter++;
                lblStatus.Text = "Encoding Video " + counter + " of " + lstVideos.Items.Count;

                // Convert the video to a temporary format before running through DSVideo.
                if (oneVideo.SubItems[3].Text.Length > 0)
                {
                    System.Diagnostics.ProcessStartInfo secondEncoderProcess = new System.Diagnostics.ProcessStartInfo(txtSecondVideoEncoder.Text);
                    secondEncoderProcess.Arguments = oneVideo.SubItems[4].Text;
                    secondEncoderProcess.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
                    System.Diagnostics.Process secondEncoderInstance;
                    secondEncoderInstance = System.Diagnostics.Process.Start(secondEncoderProcess);
                    while (secondEncoderInstance.HasExited == false)
                        Application.DoEvents();
                }

                // Execute the DSVideo encoder with the specified parameters.
                System.Diagnostics.ProcessStartInfo dsVideoProcess = new System.Diagnostics.ProcessStartInfo (Application.StartupPath + "\\dsvideo.exe");
                dsvideoParams = "-o \"" + oneVideo.SubItems[1].Text + "\" \"" + oneVideo.Text + "\"";
                if (oneVideo.SubItems[2].Text.Length > 0)
                    dsvideoParams = oneVideo.SubItems[2].Text + " " + dsvideoParams;
                dsVideoProcess.Arguments = dsvideoParams;
                dsVideoProcess.UseShellExecute = false;
                dsVideoProcess.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                dsVideoProcess.CreateNoWindow = true;
                dsVideoProcess.RedirectStandardOutput = true;
                System.Diagnostics.Process dsVideoInstance;
                
                dsVideoInstance = System.Diagnostics.Process.Start(dsVideoProcess);
                System.IO.StreamReader dsVideoOutput = dsVideoInstance.StandardOutput;

                // Wait for the encoder to exit before moving to the next video.
                while (dsVideoInstance.HasExited == false)
                {
                    Application.DoEvents();

                    // Attempt to determine what % of the video has been encoded and
                    //  display this percentage.
                    try
                    {
                        string encodeProgress = dsVideoOutput.ReadLine();
                        string progressValue;
                        if (encodeProgress.StartsWith("Progress:"))
                        {
                            progressValue = encodeProgress.Substring("Progress:".Length, encodeProgress.IndexOf("%") - "Progress:".Length).Trim();
                            progressEncode.Value = Convert.ToInt32(progressValue);
                            this.Text = "DSVE-GUI (" + progressValue + "%)";
                        }
                        else if (encodeProgress.Trim().Length > 0)
                            MessageBox.Show(encodeProgress, "Error", MessageBoxButtons.OK);
                    }
                    catch
                    {
                    }
                }
            }

            // All Done.
            buttonAdd.Enabled = true;
            buttonEncodeVideos.Enabled = true;
            progressEncode.Value = 0;
            this.Text = "DSVE-GUI";
            lstVideos.Items.Clear();
            lblStatus.Text = "Done!";
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            // Generate and display copyright text.
            string aboutText = "DSVE-GUI Version 2.1.0\n";
            aboutText += "Copyright 2008 Eric Cavaliere\n\n";
            aboutText += "This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\n";
            aboutText +="This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n\n";
            aboutText += "You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.";
            MessageBox.Show(aboutText, "About", MessageBoxButtons.OK);
        }

        private void checkUse2Encoders_CheckedChanged(object sender, EventArgs e)
        {
            groupSecondEncoder.Enabled = checkUse2Encoders.Checked;
        }

        private void buttonSelectSecondEncoder_Click(object sender, EventArgs e)
        {
            // Create an OpenFileDialog and allow the user to select a 
            //  alternate video encoder.
            OpenFileDialog encoderFile = new OpenFileDialog();
            DialogResult encoderResult;

            encoderFile.Title = "Please Select a Video Encoder";
            encoderResult = encoderFile.ShowDialog();
            if (encoderResult == DialogResult.OK)
            {
                txtSecondVideoEncoder.Text = encoderFile.FileName;
            }

        }

        private void buttonCreateTempVideo_Click(object sender, EventArgs e)
        {
            // Create a new SaveFileDialog and allow the user to
            //  specify a temporary video file file.
            SaveFileDialog outputTempFile = new SaveFileDialog();
            DialogResult outputTempResult;

            outputTempFile.Title = "Please Specify a Temporary Video File";
            outputTempResult = outputTempFile.ShowDialog();
            if (outputTempResult == DialogResult.OK)
            {
                txtTempVideoFile.Text = outputTempFile.FileName;
            }
        }

        private void buttonBuildParameters_Click(object sender, EventArgs e)
        {
            // Link the parameters window into the parameters text box and display.
            DSVideoParameters parametersWindow = new DSVideoParameters();
            parametersWindow.dsParameters = txtOptionalParams;
            parametersWindow.Show();
            parametersWindow.Left = this.Left + this.Width + 20;
            parametersWindow.Top = this.Top + 50;
        }

        private void loadPreferences()
        {
            // Load any saved preferences.
            DataSet config = new DataSet();
            DataRow[] results;

            // Load XML Schema for preferences file.
            config.ReadXmlSchema(Application.StartupPath + "\\config.xsd");

            // Check and see if preferences folder exists, create it if it does not.
            if (!System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI"))
                System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI");

            // If preferences file exists, load it.
            if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI\\config.xml"))
                config.ReadXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI\\config.xml");

            results = config.Tables[0].Select("ConfigName = 'DSVideoParameters'");
            if (results.Length > 0)
            {
                txtOptionalParams.Text = results[0].ItemArray[1].ToString();
            }
            else
            {
                DataRow newOption = config.Tables[0].NewRow();
                newOption["ConfigName"] = "DSVideoParameters";
                newOption["ConfigValue"] = txtOptionalParams.Text;
                config.Tables[0].Rows.Add(newOption);
                config.AcceptChanges();
            }

            results = config.Tables[0].Select("ConfigName = 'SecondEncoderPath'");
            if (results.Length > 0)
            {
                txtSecondVideoEncoder.Text = results[0].ItemArray[1].ToString();
            }
            else
            {
                DataRow newOption;
                newOption = config.Tables[0].NewRow();
                newOption["ConfigName"] = "SecondEncoderPath";
                newOption["ConfigValue"] = txtSecondVideoEncoder.Text;
                config.Tables[0].Rows.Add(newOption);
            }

            results = config.Tables[0].Select("ConfigName = 'SecondEncoderParameters'");
            if (results.Length > 0)
            {
                txtSecondEncoderParams.Text = results[0].ItemArray[1].ToString();
            }
            else
            {
                DataRow newOption;
                newOption = config.Tables[0].NewRow();
                newOption["ConfigName"] = "SecondEncoderParameters";
                newOption["ConfigValue"] = txtSecondEncoderParams.Text;
                config.Tables[0].Rows.Add(newOption);
            }

            results = config.Tables[0].Select("ConfigName = 'TempVideo'");
            if (results.Length > 0)
            {
                txtTempVideoFile.Text = results[0].ItemArray[1].ToString();
            }
            else
            {
                DataRow newOption;
                newOption = config.Tables[0].NewRow();
                newOption["ConfigName"] = "TempVideo";
                newOption["ConfigValue"] = txtTempVideoFile.Text;
                config.Tables[0].Rows.Add(newOption);
            }

            // If preferences files does not exist, create it
            if (!System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI\\config.xml"))
            {
                config.AcceptChanges();
                config.WriteXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI\\config.xml");
            }
        }

        private void savePreferences(object sender, CancelEventArgs cArgs)
        {
            // Save a few of the text boxes to a config file before quitting.
            DataSet config = new DataSet();
            DataRow[] results;

            // Load XML Schema for preferences file.
            config.ReadXmlSchema(Application.StartupPath + "\\config.xsd");

            // Load old XML data.
            config.ReadXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI\\config.xml");

            // Update the data with any new settings.
            results = config.Tables[0].Select("ConfigName = 'DSVideoParameters'");
            if (results.Length > 0)
                results[0][1] = txtOptionalParams.Text;

            results = config.Tables[0].Select("ConfigName = 'SecondEncoderPath'");
            if (results.Length > 0)
                results[0][1] = txtSecondVideoEncoder.Text;

            results = config.Tables[0].Select("ConfigName = 'SecondEncoderParameters'");
            if (results.Length > 0)
                results[0][1] = txtSecondEncoderParams.Text;

            results = config.Tables[0].Select("ConfigName = 'TempVideo'");
            if (results.Length > 0)
                results[0][1] = txtTempVideoFile.Text;

            // Save the new values back to the xml file.
            config.AcceptChanges();
            config.WriteXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + "\\DSVE-GUI\\config.xml");

            // Quit.
            cArgs.Cancel = false; 
        }
    }
}
