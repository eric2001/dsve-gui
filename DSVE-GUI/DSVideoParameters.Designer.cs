﻿namespace DSVE_GUI
{
    partial class DSVideoParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkLetterbox = new System.Windows.Forms.CheckBox();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.checkMaxFrames = new System.Windows.Forms.CheckBox();
            this.txtMaxFrames = new System.Windows.Forms.TextBox();
            this.checkMinBitrate = new System.Windows.Forms.CheckBox();
            this.txtMinBitrate = new System.Windows.Forms.TextBox();
            this.checkMaxBitrate = new System.Windows.Forms.CheckBox();
            this.txtMaxBitrate = new System.Windows.Forms.TextBox();
            this.checkScaleFactor = new System.Windows.Forms.CheckBox();
            this.txtScaleFactor = new System.Windows.Forms.TextBox();
            this.checkKeyframeInterval = new System.Windows.Forms.CheckBox();
            this.txtKeyframeInterval = new System.Windows.Forms.TextBox();
            this.lblKeyframeSeconds = new System.Windows.Forms.Label();
            this.checkLowerFramerate = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // checkLetterbox
            // 
            this.checkLetterbox.AutoSize = true;
            this.checkLetterbox.Location = new System.Drawing.Point(12, 5);
            this.checkLetterbox.Name = "checkLetterbox";
            this.checkLetterbox.Size = new System.Drawing.Size(83, 17);
            this.checkLetterbox.TabIndex = 0;
            this.checkLetterbox.Text = "16:9 Video?";
            this.checkLetterbox.UseVisualStyleBackColor = true;
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(113, 215);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(75, 23);
            this.buttonGenerate.TabIndex = 1;
            this.buttonGenerate.Text = "Ok";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // checkMaxFrames
            // 
            this.checkMaxFrames.AutoSize = true;
            this.checkMaxFrames.Location = new System.Drawing.Point(12, 25);
            this.checkMaxFrames.Name = "checkMaxFrames";
            this.checkMaxFrames.Size = new System.Drawing.Size(214, 17);
            this.checkMaxFrames.TabIndex = 2;
            this.checkMaxFrames.Text = "Maximum Number of Frames to Encode:";
            this.checkMaxFrames.UseVisualStyleBackColor = true;
            this.checkMaxFrames.CheckedChanged += new System.EventHandler(this.checkMaxFrames_CheckedChanged);
            // 
            // txtMaxFrames
            // 
            this.txtMaxFrames.Enabled = false;
            this.txtMaxFrames.Location = new System.Drawing.Point(223, 22);
            this.txtMaxFrames.Name = "txtMaxFrames";
            this.txtMaxFrames.Size = new System.Drawing.Size(71, 20);
            this.txtMaxFrames.TabIndex = 3;
            // 
            // checkMinBitrate
            // 
            this.checkMinBitrate.AutoSize = true;
            this.checkMinBitrate.Location = new System.Drawing.Point(12, 58);
            this.checkMinBitrate.Name = "checkMinBitrate";
            this.checkMinBitrate.Size = new System.Drawing.Size(103, 17);
            this.checkMinBitrate.TabIndex = 4;
            this.checkMinBitrate.Text = "Minimum Bitrate:";
            this.checkMinBitrate.UseVisualStyleBackColor = true;
            this.checkMinBitrate.CheckedChanged += new System.EventHandler(this.checkMinBitrate_CheckedChanged);
            // 
            // txtMinBitrate
            // 
            this.txtMinBitrate.Enabled = false;
            this.txtMinBitrate.Location = new System.Drawing.Point(121, 55);
            this.txtMinBitrate.Name = "txtMinBitrate";
            this.txtMinBitrate.Size = new System.Drawing.Size(71, 20);
            this.txtMinBitrate.TabIndex = 5;
            this.txtMinBitrate.Text = "12000";
            // 
            // checkMaxBitrate
            // 
            this.checkMaxBitrate.AutoSize = true;
            this.checkMaxBitrate.Location = new System.Drawing.Point(12, 81);
            this.checkMaxBitrate.Name = "checkMaxBitrate";
            this.checkMaxBitrate.Size = new System.Drawing.Size(106, 17);
            this.checkMaxBitrate.TabIndex = 6;
            this.checkMaxBitrate.Text = "Maximum Bitrate:";
            this.checkMaxBitrate.UseVisualStyleBackColor = true;
            this.checkMaxBitrate.CheckedChanged += new System.EventHandler(this.checkMaxBitrate_CheckedChanged);
            // 
            // txtMaxBitrate
            // 
            this.txtMaxBitrate.Enabled = false;
            this.txtMaxBitrate.Location = new System.Drawing.Point(121, 79);
            this.txtMaxBitrate.Name = "txtMaxBitrate";
            this.txtMaxBitrate.Size = new System.Drawing.Size(71, 20);
            this.txtMaxBitrate.TabIndex = 7;
            this.txtMaxBitrate.Text = "70000";
            // 
            // checkScaleFactor
            // 
            this.checkScaleFactor.AutoSize = true;
            this.checkScaleFactor.Location = new System.Drawing.Point(12, 116);
            this.checkScaleFactor.Name = "checkScaleFactor";
            this.checkScaleFactor.Size = new System.Drawing.Size(89, 17);
            this.checkScaleFactor.TabIndex = 8;
            this.checkScaleFactor.Text = "Scale Factor:";
            this.checkScaleFactor.UseVisualStyleBackColor = true;
            this.checkScaleFactor.CheckedChanged += new System.EventHandler(this.checkScaleFactor_CheckedChanged);
            // 
            // txtScaleFactor
            // 
            this.txtScaleFactor.Enabled = false;
            this.txtScaleFactor.Location = new System.Drawing.Point(121, 113);
            this.txtScaleFactor.Name = "txtScaleFactor";
            this.txtScaleFactor.Size = new System.Drawing.Size(71, 20);
            this.txtScaleFactor.TabIndex = 9;
            this.txtScaleFactor.Text = "1.5";
            // 
            // checkKeyframeInterval
            // 
            this.checkKeyframeInterval.AutoSize = true;
            this.checkKeyframeInterval.Location = new System.Drawing.Point(12, 139);
            this.checkKeyframeInterval.Name = "checkKeyframeInterval";
            this.checkKeyframeInterval.Size = new System.Drawing.Size(111, 17);
            this.checkKeyframeInterval.TabIndex = 10;
            this.checkKeyframeInterval.Text = "Keyframe Interval:";
            this.checkKeyframeInterval.UseVisualStyleBackColor = true;
            this.checkKeyframeInterval.CheckedChanged += new System.EventHandler(this.checkKeyframeInterval_CheckedChanged);
            // 
            // txtKeyframeInterval
            // 
            this.txtKeyframeInterval.Enabled = false;
            this.txtKeyframeInterval.Location = new System.Drawing.Point(121, 137);
            this.txtKeyframeInterval.Name = "txtKeyframeInterval";
            this.txtKeyframeInterval.Size = new System.Drawing.Size(71, 20);
            this.txtKeyframeInterval.TabIndex = 11;
            this.txtKeyframeInterval.Text = "5";
            // 
            // lblKeyframeSeconds
            // 
            this.lblKeyframeSeconds.AutoSize = true;
            this.lblKeyframeSeconds.Location = new System.Drawing.Point(196, 140);
            this.lblKeyframeSeconds.Name = "lblKeyframeSeconds";
            this.lblKeyframeSeconds.Size = new System.Drawing.Size(47, 13);
            this.lblKeyframeSeconds.TabIndex = 12;
            this.lblKeyframeSeconds.Text = "seconds";
            // 
            // checkLowerFramerate
            // 
            this.checkLowerFramerate.Location = new System.Drawing.Point(12, 169);
            this.checkLowerFramerate.Name = "checkLowerFramerate";
            this.checkLowerFramerate.Size = new System.Drawing.Size(280, 31);
            this.checkLowerFramerate.TabIndex = 13;
            this.checkLowerFramerate.Text = "Use 8 FPS Framerate (only available for 24 and 25 fps source videos)";
            this.checkLowerFramerate.UseVisualStyleBackColor = true;
            // 
            // DSVideoParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 247);
            this.Controls.Add(this.checkLowerFramerate);
            this.Controls.Add(this.lblKeyframeSeconds);
            this.Controls.Add(this.txtKeyframeInterval);
            this.Controls.Add(this.checkKeyframeInterval);
            this.Controls.Add(this.txtScaleFactor);
            this.Controls.Add(this.checkScaleFactor);
            this.Controls.Add(this.txtMaxBitrate);
            this.Controls.Add(this.checkMaxBitrate);
            this.Controls.Add(this.txtMinBitrate);
            this.Controls.Add(this.checkMinBitrate);
            this.Controls.Add(this.txtMaxFrames);
            this.Controls.Add(this.checkMaxFrames);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.checkLetterbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DSVideoParameters";
            this.Text = "DSVideo Parameters";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkLetterbox;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.CheckBox checkMaxFrames;
        private System.Windows.Forms.TextBox txtMaxFrames;
        private System.Windows.Forms.CheckBox checkMinBitrate;
        private System.Windows.Forms.TextBox txtMinBitrate;
        private System.Windows.Forms.CheckBox checkMaxBitrate;
        private System.Windows.Forms.TextBox txtMaxBitrate;
        private System.Windows.Forms.CheckBox checkScaleFactor;
        private System.Windows.Forms.TextBox txtScaleFactor;
        private System.Windows.Forms.CheckBox checkKeyframeInterval;
        private System.Windows.Forms.TextBox txtKeyframeInterval;
        private System.Windows.Forms.Label lblKeyframeSeconds;
        private System.Windows.Forms.CheckBox checkLowerFramerate;
    }
}