﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DSVE_GUI
{
    public partial class DSVideoParameters : Form
    {
        public DSVideoParameters()
        {
            InitializeComponent();
        }

        public TextBox dsParameters;

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            // Clear out any existing text.
            dsParameters.Text = "";

            // Generate the parameters string.
            if (checkLetterbox.Checked == true)
                dsParameters.Text = (dsParameters.Text + " -l").Trim();
            if (checkMaxFrames.Checked == true)
                dsParameters.Text = (dsParameters.Text + " -f " + txtMaxFrames.Text).Trim();
            if (checkMinBitrate.Checked == true)
                dsParameters.Text = (dsParameters.Text + " -n " + txtMinBitrate.Text).Trim();
            if (checkMaxBitrate.Checked == true)
                dsParameters.Text = (dsParameters.Text + " -x " + txtMaxBitrate.Text).Trim();
            if (checkScaleFactor.Checked == true)
                dsParameters.Text = (dsParameters.Text + " -r " + txtScaleFactor.Text).Trim();
            if (checkKeyframeInterval.Checked == true)
                dsParameters.Text = (dsParameters.Text + " -k " + txtKeyframeInterval.Text).Trim();
            if (checkLowerFramerate.Checked == true)
                dsParameters.Text = (dsParameters.Text + " -w").Trim();

            // Close this window.
            this.Close();
        }

        private void checkMaxFrames_CheckedChanged(object sender, EventArgs e)
        {
            txtMaxFrames.Enabled = checkMaxFrames.Checked;
        }

        private void checkMinBitrate_CheckedChanged(object sender, EventArgs e)
        {
            txtMinBitrate.Enabled = checkMinBitrate.Checked;
        }

        private void checkMaxBitrate_CheckedChanged(object sender, EventArgs e)
        {
            txtMaxBitrate.Enabled = checkMaxBitrate.Checked;
        }

        private void checkScaleFactor_CheckedChanged(object sender, EventArgs e)
        {
            txtScaleFactor.Enabled = checkScaleFactor.Checked;
        }

        private void checkKeyframeInterval_CheckedChanged(object sender, EventArgs e)
        {
            txtKeyframeInterval.Enabled = checkKeyframeInterval.Checked;
        }

    }
}
