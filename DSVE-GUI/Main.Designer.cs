﻿namespace DSVE_GUI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInput = new System.Windows.Forms.Label();
            this.lblOutput = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.buttonInputBrowse = new System.Windows.Forms.Button();
            this.buttonOutputBrowse = new System.Windows.Forms.Button();
            this.lstVideos = new System.Windows.Forms.ListView();
            this.colInput = new System.Windows.Forms.ColumnHeader();
            this.colOutput = new System.Windows.Forms.ColumnHeader();
            this.colParams = new System.Windows.Forms.ColumnHeader();
            this.colSecondEncoder = new System.Windows.Forms.ColumnHeader();
            this.colSecondEncoderParams = new System.Windows.Forms.ColumnHeader();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonEncodeVideos = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.progressEncode = new System.Windows.Forms.ProgressBar();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.lblDiv = new System.Windows.Forms.Label();
            this.lblOptParams = new System.Windows.Forms.Label();
            this.txtOptionalParams = new System.Windows.Forms.TextBox();
            this.groupAddVideo = new System.Windows.Forms.GroupBox();
            this.buttonBuildParameters = new System.Windows.Forms.Button();
            this.checkUse2Encoders = new System.Windows.Forms.CheckBox();
            this.groupSecondEncoder = new System.Windows.Forms.GroupBox();
            this.buttonCreateTempVideo = new System.Windows.Forms.Button();
            this.buttonSelectSecondEncoder = new System.Windows.Forms.Button();
            this.txtTempVideoFile = new System.Windows.Forms.TextBox();
            this.txtSecondEncoderParams = new System.Windows.Forms.TextBox();
            this.txtSecondVideoEncoder = new System.Windows.Forms.TextBox();
            this.lblTempVideoFile = new System.Windows.Forms.Label();
            this.lblSecondEncoderParams = new System.Windows.Forms.Label();
            this.lblSecondEncoder = new System.Windows.Forms.Label();
            this.groupAddVideo.SuspendLayout();
            this.groupSecondEncoder.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInput
            // 
            this.lblInput.AutoSize = true;
            this.lblInput.Location = new System.Drawing.Point(7, 16);
            this.lblInput.Name = "lblInput";
            this.lblInput.Size = new System.Drawing.Size(64, 13);
            this.lblInput.TabIndex = 0;
            this.lblInput.Text = "Input Video:";
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(7, 44);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(72, 13);
            this.lblOutput.TabIndex = 1;
            this.lblOutput.Text = "Output Video:";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "DSVideo Encoder Graphical User Interface.";
            // 
            // txtInput
            // 
            this.txtInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInput.Location = new System.Drawing.Point(120, 11);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(166, 20);
            this.txtInput.TabIndex = 3;
            // 
            // txtOutput
            // 
            this.txtOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutput.Location = new System.Drawing.Point(120, 40);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(166, 20);
            this.txtOutput.TabIndex = 4;
            // 
            // buttonInputBrowse
            // 
            this.buttonInputBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInputBrowse.Location = new System.Drawing.Point(289, 11);
            this.buttonInputBrowse.Name = "buttonInputBrowse";
            this.buttonInputBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonInputBrowse.TabIndex = 5;
            this.buttonInputBrowse.Text = "Browse...";
            this.buttonInputBrowse.UseVisualStyleBackColor = true;
            this.buttonInputBrowse.Click += new System.EventHandler(this.buttonInputBrowse_Click);
            // 
            // buttonOutputBrowse
            // 
            this.buttonOutputBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOutputBrowse.Location = new System.Drawing.Point(289, 40);
            this.buttonOutputBrowse.Name = "buttonOutputBrowse";
            this.buttonOutputBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonOutputBrowse.TabIndex = 6;
            this.buttonOutputBrowse.Text = "Browse...";
            this.buttonOutputBrowse.UseVisualStyleBackColor = true;
            this.buttonOutputBrowse.Click += new System.EventHandler(this.buttonOutputBrowse_Click);
            // 
            // lstVideos
            // 
            this.lstVideos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstVideos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colInput,
            this.colOutput,
            this.colParams,
            this.colSecondEncoder,
            this.colSecondEncoderParams});
            this.lstVideos.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstVideos.Location = new System.Drawing.Point(5, 302);
            this.lstVideos.Name = "lstVideos";
            this.lstVideos.Size = new System.Drawing.Size(373, 115);
            this.lstVideos.TabIndex = 7;
            this.lstVideos.UseCompatibleStateImageBehavior = false;
            this.lstVideos.View = System.Windows.Forms.View.Details;
            // 
            // colInput
            // 
            this.colInput.Text = "Input Videos";
            this.colInput.Width = 159;
            // 
            // colOutput
            // 
            this.colOutput.Text = "Output Videos";
            this.colOutput.Width = 146;
            // 
            // colParams
            // 
            this.colParams.Text = "Parameters";
            // 
            // colSecondEncoder
            // 
            this.colSecondEncoder.Text = "Second Encoder";
            // 
            // colSecondEncoderParams
            // 
            this.colSecondEncoderParams.Text = "Parameters";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.Location = new System.Drawing.Point(289, 103);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 8;
            this.buttonAdd.Text = "Add >>";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonEncodeVideos
            // 
            this.buttonEncodeVideos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEncodeVideos.Location = new System.Drawing.Point(296, 423);
            this.buttonEncodeVideos.Name = "buttonEncodeVideos";
            this.buttonEncodeVideos.Size = new System.Drawing.Size(75, 23);
            this.buttonEncodeVideos.TabIndex = 9;
            this.buttonEncodeVideos.Text = "Start";
            this.buttonEncodeVideos.UseVisualStyleBackColor = true;
            this.buttonEncodeVideos.Click += new System.EventHandler(this.buttonEncodeVideos_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(2, 428);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(38, 13);
            this.lblStatus.TabIndex = 10;
            this.lblStatus.Text = "Ready";
            // 
            // progressEncode
            // 
            this.progressEncode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressEncode.Location = new System.Drawing.Point(138, 423);
            this.progressEncode.Name = "progressEncode";
            this.progressEncode.Size = new System.Drawing.Size(141, 23);
            this.progressEncode.Step = 1;
            this.progressEncode.TabIndex = 11;
            // 
            // buttonAbout
            // 
            this.buttonAbout.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonAbout.Location = new System.Drawing.Point(154, 38);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(75, 23);
            this.buttonAbout.TabIndex = 12;
            this.buttonAbout.Text = "About";
            this.buttonAbout.UseVisualStyleBackColor = true;
            this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
            // 
            // lblCopyright
            // 
            this.lblCopyright.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Location = new System.Drawing.Point(118, 22);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(146, 13);
            this.lblCopyright.TabIndex = 13;
            this.lblCopyright.Text = "Copyright 2008 Eric Cavaliere";
            // 
            // lblDiv
            // 
            this.lblDiv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDiv.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblDiv.Location = new System.Drawing.Point(33, 64);
            this.lblDiv.Name = "lblDiv";
            this.lblDiv.Size = new System.Drawing.Size(317, 10);
            this.lblDiv.TabIndex = 14;
            // 
            // lblOptParams
            // 
            this.lblOptParams.AutoSize = true;
            this.lblOptParams.Location = new System.Drawing.Point(7, 69);
            this.lblOptParams.Name = "lblOptParams";
            this.lblOptParams.Size = new System.Drawing.Size(105, 13);
            this.lblOptParams.TabIndex = 15;
            this.lblOptParams.Text = "Optional Parameters:";
            // 
            // txtOptionalParams
            // 
            this.txtOptionalParams.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOptionalParams.Location = new System.Drawing.Point(120, 66);
            this.txtOptionalParams.Name = "txtOptionalParams";
            this.txtOptionalParams.Size = new System.Drawing.Size(166, 20);
            this.txtOptionalParams.TabIndex = 16;
            // 
            // groupAddVideo
            // 
            this.groupAddVideo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupAddVideo.Controls.Add(this.buttonBuildParameters);
            this.groupAddVideo.Controls.Add(this.checkUse2Encoders);
            this.groupAddVideo.Controls.Add(this.lblInput);
            this.groupAddVideo.Controls.Add(this.txtOptionalParams);
            this.groupAddVideo.Controls.Add(this.lblOutput);
            this.groupAddVideo.Controls.Add(this.lblOptParams);
            this.groupAddVideo.Controls.Add(this.txtInput);
            this.groupAddVideo.Controls.Add(this.txtOutput);
            this.groupAddVideo.Controls.Add(this.buttonInputBrowse);
            this.groupAddVideo.Controls.Add(this.buttonOutputBrowse);
            this.groupAddVideo.Controls.Add(this.buttonAdd);
            this.groupAddVideo.Location = new System.Drawing.Point(8, 77);
            this.groupAddVideo.Name = "groupAddVideo";
            this.groupAddVideo.Size = new System.Drawing.Size(366, 132);
            this.groupAddVideo.TabIndex = 17;
            this.groupAddVideo.TabStop = false;
            this.groupAddVideo.Text = "Add Video";
            // 
            // buttonBuildParameters
            // 
            this.buttonBuildParameters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBuildParameters.Location = new System.Drawing.Point(289, 66);
            this.buttonBuildParameters.Name = "buttonBuildParameters";
            this.buttonBuildParameters.Size = new System.Drawing.Size(75, 23);
            this.buttonBuildParameters.TabIndex = 18;
            this.buttonBuildParameters.Text = "More >>";
            this.buttonBuildParameters.UseVisualStyleBackColor = true;
            this.buttonBuildParameters.Click += new System.EventHandler(this.buttonBuildParameters_Click);
            // 
            // checkUse2Encoders
            // 
            this.checkUse2Encoders.AutoSize = true;
            this.checkUse2Encoders.Location = new System.Drawing.Point(10, 101);
            this.checkUse2Encoders.Name = "checkUse2Encoders";
            this.checkUse2Encoders.Size = new System.Drawing.Size(164, 17);
            this.checkUse2Encoders.TabIndex = 17;
            this.checkUse2Encoders.Text = "Use Second Video Encoder?";
            this.checkUse2Encoders.UseVisualStyleBackColor = true;
            this.checkUse2Encoders.CheckedChanged += new System.EventHandler(this.checkUse2Encoders_CheckedChanged);
            // 
            // groupSecondEncoder
            // 
            this.groupSecondEncoder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupSecondEncoder.Controls.Add(this.buttonCreateTempVideo);
            this.groupSecondEncoder.Controls.Add(this.buttonSelectSecondEncoder);
            this.groupSecondEncoder.Controls.Add(this.txtTempVideoFile);
            this.groupSecondEncoder.Controls.Add(this.txtSecondEncoderParams);
            this.groupSecondEncoder.Controls.Add(this.txtSecondVideoEncoder);
            this.groupSecondEncoder.Controls.Add(this.lblTempVideoFile);
            this.groupSecondEncoder.Controls.Add(this.lblSecondEncoderParams);
            this.groupSecondEncoder.Controls.Add(this.lblSecondEncoder);
            this.groupSecondEncoder.Enabled = false;
            this.groupSecondEncoder.Location = new System.Drawing.Point(8, 210);
            this.groupSecondEncoder.Name = "groupSecondEncoder";
            this.groupSecondEncoder.Size = new System.Drawing.Size(366, 88);
            this.groupSecondEncoder.TabIndex = 18;
            this.groupSecondEncoder.TabStop = false;
            this.groupSecondEncoder.Text = "Second Video Encoder";
            // 
            // buttonCreateTempVideo
            // 
            this.buttonCreateTempVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCreateTempVideo.Location = new System.Drawing.Point(289, 59);
            this.buttonCreateTempVideo.Name = "buttonCreateTempVideo";
            this.buttonCreateTempVideo.Size = new System.Drawing.Size(75, 23);
            this.buttonCreateTempVideo.TabIndex = 7;
            this.buttonCreateTempVideo.Text = "Browse...";
            this.buttonCreateTempVideo.UseVisualStyleBackColor = true;
            this.buttonCreateTempVideo.Click += new System.EventHandler(this.buttonCreateTempVideo_Click);
            // 
            // buttonSelectSecondEncoder
            // 
            this.buttonSelectSecondEncoder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectSecondEncoder.Location = new System.Drawing.Point(289, 11);
            this.buttonSelectSecondEncoder.Name = "buttonSelectSecondEncoder";
            this.buttonSelectSecondEncoder.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectSecondEncoder.TabIndex = 6;
            this.buttonSelectSecondEncoder.Text = "Browse...";
            this.buttonSelectSecondEncoder.UseVisualStyleBackColor = true;
            this.buttonSelectSecondEncoder.Click += new System.EventHandler(this.buttonSelectSecondEncoder_Click);
            // 
            // txtTempVideoFile
            // 
            this.txtTempVideoFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTempVideoFile.Location = new System.Drawing.Point(120, 61);
            this.txtTempVideoFile.Name = "txtTempVideoFile";
            this.txtTempVideoFile.Size = new System.Drawing.Size(166, 20);
            this.txtTempVideoFile.TabIndex = 5;
            this.txtTempVideoFile.Text = "D:\\temp\\temp.mpg";
            // 
            // txtSecondEncoderParams
            // 
            this.txtSecondEncoderParams.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecondEncoderParams.Location = new System.Drawing.Point(120, 35);
            this.txtSecondEncoderParams.Name = "txtSecondEncoderParams";
            this.txtSecondEncoderParams.Size = new System.Drawing.Size(166, 20);
            this.txtSecondEncoderParams.TabIndex = 4;
            this.txtSecondEncoderParams.Text = "\"%SOURCEVIDEO%\" -oac mp3lame  -ovc lavc -lavcopts vcodec=mpeg2video:autoaspect:vb" +
                "itrate=1800 -of mpeg -o \"%TEMPVIDEO%\"";
            // 
            // txtSecondVideoEncoder
            // 
            this.txtSecondVideoEncoder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecondVideoEncoder.Location = new System.Drawing.Point(120, 11);
            this.txtSecondVideoEncoder.Name = "txtSecondVideoEncoder";
            this.txtSecondVideoEncoder.Size = new System.Drawing.Size(166, 20);
            this.txtSecondVideoEncoder.TabIndex = 3;
            this.txtSecondVideoEncoder.Text = "C:\\Program Files (x86)\\mplayer\\mencoder.exe";
            // 
            // lblTempVideoFile
            // 
            this.lblTempVideoFile.AutoSize = true;
            this.lblTempVideoFile.Location = new System.Drawing.Point(7, 64);
            this.lblTempVideoFile.Name = "lblTempVideoFile";
            this.lblTempVideoFile.Size = new System.Drawing.Size(109, 13);
            this.lblTempVideoFile.TabIndex = 2;
            this.lblTempVideoFile.Text = "Temporary Video File:";
            // 
            // lblSecondEncoderParams
            // 
            this.lblSecondEncoderParams.AutoSize = true;
            this.lblSecondEncoderParams.Location = new System.Drawing.Point(7, 38);
            this.lblSecondEncoderParams.Name = "lblSecondEncoderParams";
            this.lblSecondEncoderParams.Size = new System.Drawing.Size(106, 13);
            this.lblSecondEncoderParams.TabIndex = 1;
            this.lblSecondEncoderParams.Text = "Encoder Parameters:";
            // 
            // lblSecondEncoder
            // 
            this.lblSecondEncoder.AutoSize = true;
            this.lblSecondEncoder.Location = new System.Drawing.Point(7, 16);
            this.lblSecondEncoder.Name = "lblSecondEncoder";
            this.lblSecondEncoder.Size = new System.Drawing.Size(80, 13);
            this.lblSecondEncoder.TabIndex = 0;
            this.lblSecondEncoder.Text = "Video Encoder:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 454);
            this.Controls.Add(this.groupSecondEncoder);
            this.Controls.Add(this.groupAddVideo);
            this.Controls.Add(this.lblDiv);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.buttonAbout);
            this.Controls.Add(this.progressEncode);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.buttonEncodeVideos);
            this.Controls.Add(this.lstVideos);
            this.Controls.Add(this.label1);
            this.Name = "frmMain";
            this.Text = "DSVE-GUI";
            this.groupAddVideo.ResumeLayout(false);
            this.groupAddVideo.PerformLayout();
            this.groupSecondEncoder.ResumeLayout(false);
            this.groupSecondEncoder.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInput;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button buttonInputBrowse;
        private System.Windows.Forms.Button buttonOutputBrowse;
        private System.Windows.Forms.ListView lstVideos;
        private System.Windows.Forms.ColumnHeader colInput;
        private System.Windows.Forms.ColumnHeader colOutput;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonEncodeVideos;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ProgressBar progressEncode;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.Label lblCopyright;
        private System.Windows.Forms.Label lblDiv;
        private System.Windows.Forms.ColumnHeader colParams;
        private System.Windows.Forms.Label lblOptParams;
        private System.Windows.Forms.TextBox txtOptionalParams;
        private System.Windows.Forms.GroupBox groupAddVideo;
        private System.Windows.Forms.CheckBox checkUse2Encoders;
        private System.Windows.Forms.GroupBox groupSecondEncoder;
        private System.Windows.Forms.Label lblTempVideoFile;
        private System.Windows.Forms.Label lblSecondEncoderParams;
        private System.Windows.Forms.Label lblSecondEncoder;
        private System.Windows.Forms.TextBox txtSecondVideoEncoder;
        private System.Windows.Forms.TextBox txtTempVideoFile;
        private System.Windows.Forms.TextBox txtSecondEncoderParams;
        private System.Windows.Forms.Button buttonSelectSecondEncoder;
        private System.Windows.Forms.Button buttonCreateTempVideo;
        private System.Windows.Forms.ColumnHeader colSecondEncoder;
        private System.Windows.Forms.ColumnHeader colSecondEncoderParams;
        private System.Windows.Forms.Button buttonBuildParameters;
    }
}

