# DSVE-GUI
![DSVE-GUI](./screenshot.jpg) 

## Description
This program provides a simple graphical interface to the command line video encoder available for [DSVideo](http://dsvideo.recoil.org/).  More information on DSVideo can be found [here](https://www.gamebrew.org/wiki/DSVideo).  

**License:** [GPL v.3](http://www.gnu.org/copyleft/gpl.html)

## Installation
To install, extract the "DSVE-GUI.exe" and "config.xsd" into the same folder as "dsvideo.exe".
 
## History
**Version 2.1.0**
> - DSVideo Encoder is now completely hidden while running.
> - Secondary encoders (such as mencoder) will start up minimized.
> - Released on 21 May 2008
>
> Download: [Version 2.1.0 Binary](/uploads/80309aa976609a4f40f96cebe933b187/DSVEGUI2.1.0Binary.zip) | [Version 2.1.0 Source](/uploads/8bff6c2349349ff9cfd101a61e7ac0a7/DSVEGUI2.1.0Source.zip)

**Version 2.0.0**
> - Added support for additional video encoders (Such as mencoder).
> - Added a screen to generate optional parameters for DSVideo.
> - Added XML Preferences file to store various settings.
> - Released on 07 May 2008
>
> Download: [Version 2.0.0 Binary](/uploads/f25b364ef573fae2ad547ad6d7693f3e/DSVEGUI2.0.0Binary.zip) | [Version 2.0.0 Source](/uploads/fb2cc5b19869b57a7c291dc225d70753/DSVEGUI2.0.0Source.zip)

**Version 1.1.0**
> - Added a field for additional DSVideo encoder options (such as minimum and maximum bit rates).
> - Released on 24 April 2008
>
> Download: [Version 1.1.0 Binary](/uploads/3642ad142befdd6ca414b316edb20d39/DSVEGUI1.1.0Binary.zip) | [Version 1.1.0 Source](/uploads/6b0420a2824191828af852c38cf688c4/DSVEGUI1.1.0Source.zip)

**Version 1.0.0**
> - Initial Release
> - Released on 23 April 2008
>
> Download: [Version 1.0.0 Binary](/uploads/175b4b4b5e673dfbc0b12442d1adab29/DSVEGUI1.0.0Binary.zip) | [Version 1.0.0 Source](/uploads/4c57281c56f69dea6b72aba146ecac4d/DSVEGUI1.0.0Source.zip)
